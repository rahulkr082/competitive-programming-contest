### Problem Statement 2

A Fibonacci like sequence satisfies the following recurrence relation:
F(N) = F(N-2) + F(N-1), N >= 2.


It is easy to see that if you specify explicitly the values of F(a) and F(b) where a and b are two indices, the whole sequence is defined.


Your task is very simple. Given two non-negative integers a and b, the values of F(a) and F(b), and two non-negative integers M and N, compute the sum (F(N) + F(N+1) + … + F(M)) mod 1000000007.


**Note:** You may assume that the given Fibonacci-like sequence has all integer values.


**Constraints**

1 <= T <= 1000

0 <= N <= M <= 109

0<=a<=200, 0<=b<=200; a != b

-32767 <= F(a) <= 32768

-32767 <= F(b) <= 32768


**Input Format:**

The first line contains an integer T (the number of test cases). Then, T lines follow with the input as follows:


a b F(a) F(b) M N

 
**Output Format:**

For each test case you have to output a single line containing the answer for the task.


**Sample Input:**
```
3
0 1 0 1 0 3
2 8 1 21 3 5
3 7 2 13 10 19
```

**Sample Output:**
```
4
10
10857
```

**Remark:** When generating test cases, ensure that all values of the sequence are integers. Also need a variety for the sample input and sample output.
