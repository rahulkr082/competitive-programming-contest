### Problem Statement 5

In the planet Venturion, there are V days in a year where V is an integral number of days. There are also Y months in a year, where each month contains D<sub>Y</sub> number of days such that the sum D<sub>1</sub> + D<sub>2</sub> + � + D<sub>Y</sub> = V.  Also, a week on Venturion consists of W number of days where W is less than or equal to any of the D<sub>i</sub>s. 

The names of each day of the week are W<sub>1</sub> , W<sub>2</sub> , � , W<sub>W</sub>.

The date is given in the form d/m/y where d is the dth day in the month m, m is the mth month of the year y, and y is the number of years passed since the adoption of this calendar system in Venturion. Note that the calendar started in the year 0 and the date at this time is 1/1/0.

(So the index of the day and month starts from 1, while the index of the year starts from 0).

Given a date in the form d/m/y as outlined above, what day of the week is it in Venturion on that date if the day of the week is W<sub>K</sub> on the date p/q/r?

**Constraints:**
T, V, Y, D<sub>i</sub>, W, K are all integers with the following constraints:

1 <= T <= 10000

1 <= V <= 500

1 <= Y <= 30

1 <= D<sub>i</sub> and  D<sub>1</sub> + � + D<sub>Y</sub> = V

1 <= W <= D<sub>i</sub>

1 <= K <= D<sub>i</sub>


W<sub>1</sub>, W<sub>2</sub>, �, W<sub>W</sub> are all strings with max length L where L <= 12.

**Input format:**

The first line of input consists of T which is the number of test cases. Then T lines follow with the input as follows:

V Y D<sub>1</sub> D<sub>2</sub> ... D<sub>Y</sub> W W<sub>1</sub> W<sub>2</sub> ... W<sub>W</sub> q r W<sub>K</sub> d m y

**Output format:**

W<sub>i</sub>

Where W<sub>i</sub> is the string corresponding to the day of the week on the date d/m/y.


**Sample Input:**
```
1
365 12 31 28 31 30 31 30 31 31 30 31 30 31 7 Sunday Monday Tuesday Wednesday Thursday Friday Saturday 1 1 1 Saturday 1 1 2
```
**Sample Output:**
```
Sunday
```